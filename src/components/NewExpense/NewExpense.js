import React, { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
    const [isAddExpense, setIsAddExpense] = useState(false);

    const saveExpenseDataHandler = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData,
            id: Math.random.toString(),
        };
        // console.log(expenseData);

        props.onAddExpense(expenseData);

        setIsAddExpense(false);
    };

    const showExpenseForm = () => {
        setIsAddExpense(true);
    };

    const hintExpenseHandler = () => {
        setIsAddExpense(false);
    };

    return (
        <div className="new-expense">
            {!isAddExpense && (
                <button onClick={showExpenseForm}>Add New Expense</button>
            )}
            {isAddExpense && (
                <ExpenseForm
                    onCancel={hintExpenseHandler}
                    onSaveExpenseData={saveExpenseDataHandler}
                />
            )}
        </div>
    );
};
export default NewExpense;
